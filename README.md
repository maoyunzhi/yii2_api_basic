<p>
    <h1 align="center">API Demo By Yii 2.0 Basic Template</h1>
</p>

基于yii2.0的basic模版，进行一定的精简后，作为后端API接口服务，抛开具体的业务现，仅考虑后端API基本的一些验证实现方式。
可以让任何接口项目，都可以基于这个模版进行后续的开发。

虽然Yii2.0一般都是基于PHP 5.6的版本，但php5的性能相对于7之后的版本，差距还是比较大的，为了拥抱新版本，将PHP的版本支持改为7.4 。

DIRECTORY STRUCTURE
------------------- 
      api_entrance        api request entrance
      assets/             contains assets definition
      commons/            contains common classes
      config/             contains application configurations
      controllers/        contains Web controller classes
      models/             contains model classes
      runtime/            contains files generated during runtime
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources


REQUIREMENTS
------------

具体实现的功能：
1. URL美化， 例如：http://XXX.com/api/version ，本模版不支持模块化的v1模式，偏向于简单化。
2. 支持返回统一的格式化数据。参考：commons\ApiResponse类
3. 签名认证， 要求URL中必须带sign和ut，sing代表签名，ut代表utc时间戳，签名的算法，参考commons\SignFilter类
4. Token认证，采用Yii2自带的HttpBearerAuth认证模式，验证Header中的token值
5. 采用独立认证类，而不是直接继承ActiveRecord类，更方便自定义认证算法。 认证类：models\ApiAuth
6. 支持获取Token方法，该方法不受Token认证限制。方便采用动态Token方案。
7. 支持获取签名方法，该方法不受签名认证，Token认证限制。方便验证签名正确性。
8. 支持API基类，业务API类只需要继承API基类，就可以支持API的基本功能。API基类：controllers\ApiController类

POSTMAN演示顺序
1. 调用获取签名接口：http://localhost/yii2_api_basic/api_entrance/api/sign ，方式和参数，使用获取Token接口需要用的参数，获取时间戳和sign
2. 调用获取Token接口：http://localhost/yii2_api_basic/api_entrance/api/get_token?sign=XXXX&ut=XXXX, 用签名接口获得的sign和时间戳替换，并在body的raw中提交app_code和app_secret，获取Token值
3. 再调用签名接口，使用将要测试的接口参数，获取时间戳和sign，例如即将调用demo接口，采用demo接口的参数
4. 再调用测试接口，如demo，用上面获取的时间戳和sign，并在header中，添加Content-Type：application/json，token:第二步获取的token值


