<?php
namespace app\commons;

class ApiResponse extends \yii\base\BaseObject
{

    private string $_statusCode;

    private string $_msg;

    private array $_data;

    const SUCCESS_CODE = 200;

    const DEFINED_ERROR_CODE = 500;

    /**
     * @return string
     */
    public function getStatusCode(): string
    {
        return $this->_statusCode;
    }

    /**
     * @param $status_code
     * @return void
     */
    public function setStatusCode($status_code):void
    {
        $this->_statusCode=$status_code;
    }

    /**
     * @return string
     */
    public function getMsg():string
    {
        return $this->_msg;
    }

    /**
     * @param string $msg
     */
    public function setMsg(string $msg): void
    {
        $this->_msg = $msg;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->_data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->_data = $data;
    }


    /**
     * @param array $data
     * @return array
     */
    public function success(array $data=[]): array
    {

        $this->setStatusCode(self::SUCCESS_CODE );
        $this->setMsg('Success');
        $this->setData($data);

        return [
            'statusCode'=> $this->_statusCode,
            'msg' => $this->_msg,
            'returnData' => $this->_data
        ];

    }

    /**
     * @param int $code
     * @param array $data
     * @return array
     */
    public function error(int $code,array $data=[])
    {

        if(YII_DEBUG){
            $this->_statusCode = $code;
            $this->_msg = "Debug Model.";
        }else {
            if ('' == $code || !isset(ApiException::$codesList[$code])) {
                $this->_statusCode = self::DEFINED_ERROR_CODE;

            }else{
                $this->_statusCode = $code;
            }
            $this->_msg = ApiException::$codesList[$this->_statusCode];
        }

        if(null == $data || [] == $data){
            $this->_data = null;
        }else{
            $this->_data = $data;
        }

        return [
            "statusCode"=>$this->_statusCode,
            "msg" => $this->_msg,
            "returnData" => $this->_data
        ];

    }

    /**
     * @return array
     */
    public function send(): array
    {
        return [
            "statusCode"=>$this->_statusCode,
            "msg" => $this->_msg,
            "returnData" => $this->_data
        ];
    }
}