<?php

namespace app\controllers;

use app\commons\ApiResponse;

class VersionController extends \yii\web\Controller
{

    /**
     * 获取版本信息
     * @return array
     */
    public function actionIndex(): array
    {
        $api_response = new ApiResponse();
        $arr_version = [
            'version'=> \Yii::$app->params['version_info']['version'],
            'time' => \Yii::$app->params['version_info']['update_time']
        ];

        return $api_response->success($arr_version);
    }
}